﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using Assistant;
using System.Windows.Forms;

namespace RazorEnhanced
{
    [StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    internal unsafe struct CartographerEntry
    {
        public byte flags;
    }

    [Flags]
    internal enum CartographerFlags : byte
    {
        IsUnexplored = 0x01,
        HasHouseDeed = 0x02
    }


    internal class CartographerPathfinder
    {
        internal class Cell
        {
            public int X;
            public int Y;
            public int Z;
            public int H;
            public Cell OnTop = null;
            public Cell Previous = null;
            public int Distance = 0;
            public int Steps = 0;
            public bool Processed = false;
            public bool IsBlocking = false;
            public bool IsFloor = false;
            public List<Cell> Links = new List<Cell>();

            public Cell(int X, int Y)
            {
                this.X = X;
                this.Y = Y;
            }
        }

        internal class Boundary
        {
            public int MinX;
            public int MaxX;
            public int MinY;
            public int MaxY;

            public Boundary(int minX, int maxX, int minY, int maxY)
            {
                MinX = Math.Max(minX, 0);
                MaxX = Math.Min(maxX, Cartographer.m_MapWidth);
                MinY = Math.Max(minY, 0);
                MaxY = Math.Min(maxY, Cartographer.m_MapHeight);
            }

            public bool InBounds(int i, int j, out int index)
            {
                index = 0;
                if (i > 0 && j > 0 && i < XRange() && j < YRange())
                {
                    index = i * YRange() + j;
                    return true;
                }
                return false;
            }

            public int XRange()
            {
                return (MaxX - MinX) + 1;
            }

            public int YRange()
            {
                return (MaxY - MinY) + 1;
            }
        }

        internal class PriorityQueue
        {
            private List<Cell> m_Data;
            private int m_Count;
            public PriorityQueue()
            {
                m_Data = new List<Cell>();
                m_Count = 0;
            }

            public void Add(Cell cell)
            {
                bool inserted = false;
                m_Data.Remove(cell);
                for (int i = 0; (i < m_Data.Count) && !inserted; i++)
                {
                    Cell inList = m_Data[i];
                    if ((cell.Steps + cell.Distance) < (inList.Steps + inList.Distance))
                    {
                        m_Data.Insert(i, cell);
                        inserted = true;
                    }
                }
                if (!inserted)
                {
                    m_Data.Add(cell);
                }
                m_Count++;
            }

            public Cell Dequeue()
            {
                if (m_Data.Count > 0)
                {
                    Cell cell = m_Data[0];
                    m_Data.Remove(cell);
                    m_Count--;
                    return cell;
                }
                return null;
            }

            public int Count
            {
                get
                {
                    return m_Data.Count;
                }
            }
        }

        internal class Graph
        {
            

            internal List<Cell> m_Cells = new List<Cell>();
            internal List<Cell> m_Grid;
            internal Boundary m_Bounds;
            internal PriorityQueue m_PriorityQueue = new PriorityQueue();
            public Graph(Boundary bounds, int mapId)
            {
                Ultima.Map map = GetMap(mapId);
                m_Bounds = bounds;
                int xRange = bounds.XRange();
                int yRange = bounds.YRange();
                m_Grid = new List<Cell>(xRange * yRange);
                for (int i = bounds.MinX; i <= bounds.MaxX; i++)
                {
                    for (int j = bounds.MinY; j <= bounds.MaxY; j++)
                    {
                        // Cell c = new Cell(i, j);
                        // c.Z = map.Tiles.GetLandTile(i, j).Z;
                        Cell c = BuildCell(map, i, j);
                        m_Grid.Add(c);
                    }
                }
                for (int i = 0; i < xRange; i++)
                {
                    for (int j = 0; j < yRange; j++)
                    {
                        int cellIndex = GetCellIndex(i, j);
                        if (m_Grid[cellIndex] != null)
                        {
                            TryAddValidCells(map, i, j, 0);
                            Cell cell = m_Grid[cellIndex].OnTop;
                            int k = 1;
                            while (cell != null)
                            {
                                TryAddValidCells(map, i, j, k);
                                cell = cell.OnTop;
                            }
                        }
                    }
                }
                // Build the graph using Mobiles info, Land info, Statics info and Deed Info
            }

            public Cell BuildCell(Ultima.Map map, int x, int y)
            {
                var landTile = map.Tiles.GetLandTile(x, y);
                var staticTiles = map.Tiles.GetStaticTiles(x, y);

                if (staticTiles.Count() == 0)
                {
                    CartographerTile tile = new CartographerTile(landTile);
                    if (tile.Impassable) return null;
                    Cell cell = new Cell(x, y);
                    cell.Z = tile.Z;
                    cell.H = tile.H;
                    return cell;
                }
                else
                {
                    List<CartographerTile> candidates = new List<CartographerTile>();
                    List<CartographerTile> impassables;
                    List<CartographerTile> surfaces;
                    List<CartographerTile> selected = new List<CartographerTile>();
                    Dictionary<int, int> levels = new Dictionary<int, int>();
                    candidates.Add(new CartographerTile(landTile));
                    foreach (var staticTile in staticTiles)
                    {
                        candidates.Add(new CartographerTile(staticTile));
                    }

                    impassables = candidates.Where(c => c.Impassable).ToList();
                    surfaces = candidates.Where(s =>
                    {
                        // int cur = s.Z + s.H;
                        int cur = s.Z;
                        bool blocked = impassables.Any(i =>
                        {
                            int top = i.Z + i.H;
                            int bot = i.Z;
                            return (cur < top && cur >= bot);
                        });
                        return (s.Ground || s.Surface) && !blocked && !s.Impassable;
                    }).OrderBy(s => s.Z).ToList();

                    for (int i = 0; i < surfaces.Count; i++)
                    {
                        CartographerTile target = surfaces[i];
                        int top = target.Z + target.H + 16;
                        int bot = target.Z;
                        bool insertable = !target.Blocked;
                        for (int j = 0; j < surfaces.Count; j++)
                        {
                            if (i != j)
                            {
                                CartographerTile other = surfaces[j];
                                if (other.Z <= top && other.Z > bot)
                                {
                                    if (other.Ground)
                                    {
                                        other.Blocked = true;
                                    }
                                    else
                                    {
                                        insertable = false;
                                    }
                                }
                            }
                        }
                        if (insertable)
                        {
                            int index;
                            if (levels.TryGetValue(target.Z, out index))
                            {
                                selected[index] = target;
                            }
                            else
                            {
                                selected.Add(target);
                                levels.Add(target.Z, selected.Count - 1);
                            }
                        }
                    }
                    //
                    if (selected.Count == 0) return null;
                    Cell cell = new Cell(x, y);
                    cell.Z = selected[0].Z;
                    cell.H = selected[0].H;
                    Cell reference = cell;
                    for (int i = 1; i < selected.Count; i++)
                    {
                        reference.OnTop = new Cell(x, y);
                        reference = reference.OnTop;
                        reference.Z = selected[i].Z;
                        reference.H = selected[i].H;
                    }
                    return cell;
                }
            }

            public Cell FindNearstCell(int x, int y, int z)
            {
                List<Cell> nearby = m_Cells.FindAll(cell =>
                {
                    // return ((cell.X == x) && (cell.Y == y));
                    // return ((cell.X == x) && (cell.Y == y) && (Math.Abs(z - cell.Z) < 3));
                    return (cell.X == x) && (cell.Y == y) && ((z - cell.Z) <= 3);
                }).ToList();
                if (nearby.Count > 0)
                {
                    int nearstIndex = 0;
                    double nearstDist = 100.0;
                    for (int i = 0; i < nearby.Count; i++)
                    {
                        double metric = (nearby[i].Z - z);
                        double squared = metric * metric;
                        if (squared < nearstDist)
                        {
                            nearstDist = squared;
                            nearstIndex = i;
                        }
                    }
                    return nearby[nearstIndex];
                }
                return null;
            }

            private bool CellIsValid(Ultima.Map map, int index, int k)
            {
                Cell cell = m_Grid[index];
                if (cell == null) return false;
                while (k-- > 0) cell = cell.OnTop;
                if (Cartographer.HasDeedAt(cell.X, cell.Y))
                    return false;
                return true;
            }

            private bool CellsAreConnected(Ultima.Map map, int sourceIndex, int sk, int targetIndex, int tk)
            {
                // TODO: Logic for connecting cells will be in here
                Cell refCell = m_Grid[sourceIndex];
                Cell targetCell = m_Grid[targetIndex];
                int origTk = tk;
                while (sk-- > 0) refCell = refCell.OnTop;
                while (tk-- > 0) targetCell = targetCell.OnTop;
                // Remake!!!
                if (targetCell.Z - (refCell.Z + refCell.H) > 3) return false;
                if (refCell.Z - (targetCell.Z + targetCell.H) > 4) return false;
                // Cartographer.MoveTo(3033, 527, 40)
                //int x1 = Math.Abs(refCell.Z + refCell.H - targetCell.Z);
                //int x2 = Math.Abs(targetCell.Z + targetCell.H - refCell.Z);
                //int xa = Math.Min(x1, x2);
                //if (xa > 3) return false;
                return CellIsValid(map, targetIndex, origTk);
            }

            private int GetCellIndex(int i, int j)
            {
                return i * m_Bounds.YRange() + j;
            }

            private void TryAddValidCells(Ultima.Map map, int i, int j, int k)
            {
                int index = GetCellIndex(i, j);
                bool useDiagonals = true;
                if (CellIsValid(map, index, k))
                {
                    bool hasNeighbors = false;
                    hasNeighbors |= TryAddAdjacentCell(map, i, j, k, -1, 0);
                    hasNeighbors |= TryAddAdjacentCell(map, i, j, k, 0, -1);
                    hasNeighbors |= TryAddAdjacentCell(map, i, j, k, 0, 1);
                    hasNeighbors |= TryAddAdjacentCell(map, i, j, k, 1, 0);
                    if (useDiagonals)
                    {
                        hasNeighbors |= TryAddAdjacentCell(map, i, j, k, -1, -1);
                        hasNeighbors |= TryAddAdjacentCell(map, i, j, k, -1, 1);
                        hasNeighbors |= TryAddAdjacentCell(map, i, j, k, 1, -1);
                        hasNeighbors |= TryAddAdjacentCell(map, i, j, k, 1, 1);
                    }
                    if (hasNeighbors)
                    {
                        Cell cell = m_Grid[index];
                        while (k-- > 0) cell = cell.OnTop;
                        m_Cells.Add(cell);
                    }
                }
            }

            private bool TryAddAdjacentCell(Ultima.Map map, int i, int j, int k, int offX, int offY)
            {
                int adjI = i + offX;
                int adjJ = j + offY;
                int origK = k;
                int adjIndex;
                int index = GetCellIndex(i, j);
                bool hasConnection = false;
                if (m_Bounds.InBounds(adjI, adjJ, out adjIndex))
                {
                    Cell source = m_Grid[index];
                    Cell target = m_Grid[adjIndex];
                    while (k-- > 0) source = source.OnTop;
                    int adjK = 0;
                    while (target != null)
                    {
                        if (CellsAreConnected(map, index, origK, adjIndex, adjK))
                        {
                            source.Links.Add(target);
                            hasConnection |= true;
                        }
                        target = target.OnTop;
                        adjK++;
                    }
                }
                return hasConnection;
            }
        }
        private Graph m_Graph;

        private static Ultima.Map GetMap(int id)
        {
            switch (id)
            {
                case 0:
                    return Ultima.Map.Felucca;
                default:
                    return null;
            }
        }

        Cell m_Source;
        Cell m_Target;
        public CartographerPathfinder(int startX, int startY, int startZ, int targetX, int targetY, int targetZ)
        {
            int minX = Math.Min(startX, targetX) - 80;
            int maxX = Math.Max(startX, targetX) + 80;
            int minY = Math.Min(startY, targetY) - 80;
            int maxY = Math.Max(startY, targetY) + 80;
            Boundary bounds = new Boundary(minX, maxX, minY, maxY);
            m_Graph = new Graph(bounds, Player.Map);
            m_Source = m_Graph.FindNearstCell(startX, startY, startZ);
            m_Target = m_Graph.FindNearstCell(targetX, targetY, targetZ);
            if (m_Source != null)
            {
                Misc.SendMessage($"Start source at Z {m_Source.Z}", 55);
            }
            if (m_Target == null)
            {
                Misc.SendMessage("Desired position is not in the grid", 33);
            }
        }

        public List<Point3D> FindRoute()
        {
            List<Point3D> route = new List<Point3D>();
            m_Graph.m_PriorityQueue.Add(m_Source);
            Cell currCell = null;
            bool founded = false;
            bool skip = m_Target == null;
            while ((m_Graph.m_PriorityQueue.Count > 0) && !founded && !skip)
            {
                currCell = m_Graph.m_PriorityQueue.Dequeue();
                if (currCell == null) continue;
                if (currCell == m_Target)
                {
                    founded = true;
                    break;
                }

                List<Cell> linkedCells = currCell.Links.Where(cell => !cell.Processed).ToList();
                foreach (Cell cell in linkedCells)
                {
                    int steps = currCell.Steps + Math.Abs(currCell.X - cell.X) + Math.Abs(currCell.Y - cell.Y);
                    if (cell.Previous == null)
                    {
                        cell.Steps = steps;
                        cell.Previous = currCell;
                        cell.Distance = Math.Max(Math.Abs(cell.X - m_Target.X), Math.Abs(cell.Y - m_Target.Y));
                        m_Graph.m_PriorityQueue.Add(cell);
                    }
                    else
                    {
                        if (steps < cell.Steps)
                        {
                            cell.Steps = steps;
                            cell.Previous = currCell;
                            m_Graph.m_PriorityQueue.Add(cell);
                        }
                    }
                }
                currCell.Processed = true;
            }
            if (founded && currCell != null)
            {
                Misc.SendMessage("Route founded", 66);
                while (currCell.Previous != null)
                {
                    route.Insert(0, new Point3D(currCell.X, currCell.Y, currCell.Z));
                    currCell = currCell.Previous;
                }
            }
            else Misc.SendMessage("Rout not founded", 33);
            return route;
        }
    }

    public class CartographerTile
    {
        public int ID;
        public int Z;
        public int H;
        public bool Ground;
        public bool Surface;
        public bool Impassable;
        public bool Blocked;

        public CartographerTile(Ultima.Tile tile)
        {
            var data = Ultima.TileData.LandTable[tile.ID];
            ID = tile.ID;
            Z = tile.Z;
            H = 0;
            Ground = true;
            Surface = data.Surface;
            Impassable = data.Impassable;
            Blocked = false;
        }

        public CartographerTile(Ultima.HuedTile tile)
        {
            var data = Ultima.TileData.ItemTable[tile.ID];
            ID = tile.ID;
            Z = tile.Z;
            H = data.CalcHeight;
            Ground = false;
            Surface = data.Surface;
            Impassable = data.Impassable;
            Blocked = false;
        }
    }

    public class Cartographer
    {
        private static Thread m_Thread = null;
        private static string m_FileName = "custom0.mul"; // add support to other maps
        internal static int m_MapWidth = 768 * 8;
        internal static int m_MapHeight = 512 * 8;
        internal static int m_MapSize = 512 * 768 * 64;
        private static CartographerEntry[] m_Entries = new CartographerEntry[m_MapSize];

        internal static void Initialize()
        {
            if (m_Thread != null) return;
            Misc.SendMessage("Initializing cartographer", 66);
            if (!File.Exists(m_FileName))
            {
                CreateFileWithDefaults();
            }
            else
            {
                using (FileStream fs = new FileStream(m_FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    byte[] buffer = new byte[m_MapSize];
                    fs.Read(buffer, 0, buffer.Length);
                    for (int i = 0; i < m_MapSize; i++)
                    {
                        m_Entries[i].flags = buffer[i];
                    }
                }
            }
            m_Thread = new Thread(SyncWorker);
            m_Thread.IsBackground = true;
            m_Thread.Start();
        }

        private unsafe static void CreateFileWithDefaults()
        {
            using (FileStream fs = new FileStream(m_FileName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                using (BinaryWriter bs = new BinaryWriter(fs))
                {
                    for (int i = 0; i < m_MapSize; i++)
                    {
                        m_Entries[i].flags = (byte)CartographerFlags.IsUnexplored;
                        bs.Write(m_Entries[i].flags);
                    }
                }
            }
        }

        private static void CheckForUpdates()
        {
            if (Player.Map == 0)
            {
                bool hasUpdates = false;
                Point3D pos = Player.Position;
                for (int i = pos.X - 15; i <= pos.X + 15; i++)
                {
                    for (int j = pos.Y - 15; j <= pos.Y + 15; j++)
                    {
                        if (i > 0 && j > 0 && i < m_MapWidth && j < m_MapHeight)
                        {
                            if (TryUpdateIsUnexplored(i, j) ||
                                TryUpdateHasDeed(i, j))
                            {
                                UpdateFileAt(i, j);
                                hasUpdates = true;
                            }
                        }
                    }
                }
                if (hasUpdates) Misc.SendMessage("Cartograph map updated", 66);
            }
        }

        private static int GetIndexAt(int x, int y)
        {
            return x * m_MapHeight + y;
        }

        private static CartographerEntry GetEntryAt(int x, int y)
        {
            return m_Entries[GetIndexAt(x, y)];
        }

        public static byte GetFlagsAt(int x, int y)
        {
            return GetEntryAt(x, y).flags;
        }

        public static bool HasDeedAt(int x, int y)
        {
            return (GetEntryAt(x, y).flags & (byte)CartographerFlags.HasHouseDeed) != 0;
        }

        public static bool IsUnexploredAt(int x, int y)
        {
            return (GetEntryAt(x, y).flags & (byte)CartographerFlags.IsUnexplored) != 0;
        }

        private static bool TryUpdateHasDeed(int x, int y)
        {
            bool hasDeed = Statics.CheckDeedHouse(x, y);
            if (HasDeedAt(x, y) != hasDeed)
            {
                int index = GetIndexAt(x, y);
                if (hasDeed) m_Entries[index].flags |= (byte)CartographerFlags.HasHouseDeed;
                else m_Entries[index].flags &= (byte)~CartographerFlags.HasHouseDeed;
                return true;
            }
            return false;
        }

        private static bool TryUpdateIsUnexplored(int x, int y)
        {
            return false;
            // TODO: See if it is usefull in the future.
            //if (IsUnexploredAt(x, y))
            //{
            //    int index = GetIndexAt(x, y);
            //    m_Entries[index].flags &= (byte) (~CartographerFlags.IsUnexplored);
            //    return true;
            //}
            //return false;
        }
        
        private static void UpdateFileAt(int x, int y)
        {
            using (FileStream fs = new FileStream(m_FileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                byte[] array = new byte[1];
                int index = GetIndexAt(x, y);
                array[0] = m_Entries[index].flags;
                fs.Seek(index, SeekOrigin.Begin);
                fs.Write(array, 0, 1);
            }
        }

        private static void SyncWorker()
        {
            while (true)
            {
                if (World.Player != null)
                {
                    CheckForUpdates();
                }
                Thread.Sleep(2000);
            }
        }

        public static List<Point3D> FindRouteTo(int x, int y, int z)
        {
            Initialize();
            Point3D pos = Player.Position;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            CartographerPathfinder pf = new CartographerPathfinder(pos.X, pos.Y, pos.Z, x, y, z);
            stopwatch.Stop();
            Misc.SendMessage($"{stopwatch.ElapsedMilliseconds} to build graph", 22);
            stopwatch.Reset();
            stopwatch.Start();
            List<Point3D> route = pf.FindRoute();
            stopwatch.Start();
            Misc.SendMessage($"{stopwatch.ElapsedMilliseconds} to find route", 22);
            return route;
        }

        public static void Describe()
        {
            int x = Player.Position.X;
            int y = Player.Position.Y;
            int z = Player.Position.Z;
            Assistant.Point3D point = new Assistant.Point3D(x, y, z);
            Engine.MainWindow.BeginInvoke((MethodInvoker)delegate
            {
                UI.EnhancedStaticInspector inspector = new UI.EnhancedStaticInspector(point);
                inspector.TopMost = true;
                inspector.Show();
            });
            Ultima.Map map = Ultima.Map.Felucca;
            var landTile = map.Tiles.GetLandTile(x, y);
            var staticTiles = map.Tiles.GetStaticTiles(x, y);

            List<CartographerTile> candidates = new List<CartographerTile>();
            List<CartographerTile> impassables;
            List<CartographerTile> surfaces;
            List<CartographerTile> selected = new List<CartographerTile>();
            Dictionary<int, int> levels = new Dictionary<int, int>();
            candidates.Add(new CartographerTile(landTile));
            foreach (var staticTile in staticTiles)
            {
                candidates.Add(new CartographerTile(staticTile));
            }

            impassables = candidates.Where(c => c.Impassable).ToList();
            surfaces = candidates.Where(s =>
            {
                int cur = s.Z + s.H;
                bool blocked = impassables.Any(i =>
                {
                    int top = i.Z + i.H;
                    int bot = i.Z; // TODO: CHECK IF -3 IS OK
                    return (cur < top && cur >= bot);
                });
                return (s.Ground || s.Surface) && !blocked;
            }).OrderBy(s => s.Z).ToList();

            for (int i = 0; i < surfaces.Count; i++)
            {
                CartographerTile target = surfaces[i];
                int top = target.Z + target.H + 16;
                int bot = target.Z;
                bool insertable = !target.Blocked;
                for (int j = 0; j < surfaces.Count; j++)
                {
                    if (i != j)
                    {
                        CartographerTile other = surfaces[j];
                        if (other.Z <= top && other.Z > bot)
                        {
                            if (other.Ground)
                            {
                                other.Blocked = true;
                            }
                            else
                            {
                                insertable = false;
                            }
                        }
                    }
                }
                if (insertable)
                {
                    int index;
                    if (levels.TryGetValue(target.Z, out index))
                    {
                        selected[index] = target;
                    }
                    else
                    {
                        selected.Add(target);
                        levels.Add(target.Z, selected.Count - 1);
                    }
                }
            }
            foreach(CartographerTile p in selected)
            {
                Misc.SendMessage($"{p.ID.ToString("X4")};{p.Z};{p.H}", 22);
            }
        }

        private static bool PlayerOnPosition(int x, int y, int z)
        {
            return Player.Position.X == x && Player.Position.Y == y;
        }

        private static void EnsureOnPosition(int x, int y, int z, int retries = 1)
        {
            int timeTick = 150;
            double maxTime = 1.5;
            double elapsedTime = 0.0;
            double timeStep = timeTick / 1000.0;
            Player.PathFindTo(x, y, z);
            while (elapsedTime < maxTime && !PlayerOnPosition(x, y, z))
            {
                Thread.Sleep(timeTick);
                elapsedTime += timeStep;
            }
            if (!PlayerOnPosition(x, y, z) && retries > 0)
            {
                Items.Filter flt = new Items.Filter();
                flt.Movable = false;
                flt.RangeMax = 3;
                List<Item> items = Items.ApplyFilter(flt);
                foreach (Item item in items)
                {
                    if (item.IsDoor)
                    {
                        Items.UseItem(item.Serial);
                    }
                }
                EnsureOnPosition(x, y, z, retries - 1);
            }
        }

        public static void MoveTo(int x, int y, int z, int stepsize = 2)
        {

            var route = FindRouteTo(x, y, z);
            if (route.Count == 0)
            {
                Misc.SendMessage("Can't get there", 33);
            }
            else
            {
                Point3D point;
                for (int i = 0; i < route.Count; i += stepsize)
                {
                    point = route[i];
                    EnsureOnPosition(point.X, point.Y, point.Z);
                }
                point = route[route.Count - 1];
                EnsureOnPosition(point.X, point.Y, point.Z);
            }
            // Opmize route (swap between run and walk)
        }
    }
}
