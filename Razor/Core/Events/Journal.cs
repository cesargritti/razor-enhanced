﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using System.Threading;
using RazorEnhanced;

namespace Assistant
{
    internal class JournalObserver
    {
        internal string Pattern = null;
        internal string Type = null;
        internal Regex RegularExpression = null;
        internal Action<Journal.JournalEntry> OnMatchCallback = null;
        internal Action<Journal.JournalEntry> OnEntryCallback = null;

        internal JournalObserver(Action<Journal.JournalEntry> callback)
        {
            OnEntryCallback = callback;
        }

        internal JournalObserver(string pattern, Action<Journal.JournalEntry> callback)
        {
            Pattern = pattern;
            RegularExpression = new Regex(pattern, RegexOptions.Compiled);
            OnMatchCallback = callback;
        }

        internal JournalObserver(string pattern, string type, Action<Journal.JournalEntry> callback)
        {
            Pattern = pattern;
            Type = type;
            RegularExpression = new Regex(pattern, RegexOptions.Compiled);
            OnMatchCallback = callback;
        }
    }

    internal class JournalEventHandler
    {   
        private static BlockingCollection<Journal.JournalEntry> m_Events;
        private static ConcurrentDictionary<int, List<JournalObserver>> m_Observers;
        private static Thread m_Thread;

        static JournalEventHandler()
        {
            m_Events = new BlockingCollection<Journal.JournalEntry>();
            m_Observers = new ConcurrentDictionary<int, List<JournalObserver>>();
            m_Thread = new Thread(AsyncRun);
            m_Thread.Start();
        }

        internal static void RegisterObserver(int threadId, JournalObserver observer)
        {
            List<JournalObserver> observers;
            if (m_Observers.TryGetValue(threadId, out observers))
            {
                observers.Add(observer);
            }
            else
            {
                observers = new List<JournalObserver>();
                observers.Add(observer);
                m_Observers.TryAdd(threadId, observers);
            }
        }

        internal static void UnregisterObservers(int threadId)
        {
            List<JournalObserver> observers;
            m_Observers.TryRemove(threadId, out observers);
        }

        private static void AsyncRun()
        {
            while (!m_Events.IsAddingCompleted)
            {
                try
                {
                    Journal.JournalEntry entry = m_Events.Take();
                    Process(entry);
                }
                catch { }
            }
        }

        private static void Process(Journal.JournalEntry entry)
        {
            foreach (KeyValuePair<int, List<JournalObserver>> pair in m_Observers)
            {
                List<JournalObserver> observers = pair.Value;
                foreach (JournalObserver observer in observers)
                {
                    observer.OnEntryCallback?.Invoke(entry);
                    if (observer.OnMatchCallback != null)
                    {
                        bool match = true;
                        if (observer.RegularExpression != null)
                        {
                            match = observer.RegularExpression.IsMatch(entry.Text);
                        }
                        if (observer.Type != null)
                        {
                            match = observer.Type == entry.Type;
                        }
                        if (match)
                        {
                            observer.OnMatchCallback(entry);
                        }
                    }
                }
            }
        }

        internal static void Stop()
        {
            m_Events.CompleteAdding();
        }

        internal static void Dispatch(Journal.JournalEntry entry)
        {
            m_Events.Add(entry);
        }
    }
}
